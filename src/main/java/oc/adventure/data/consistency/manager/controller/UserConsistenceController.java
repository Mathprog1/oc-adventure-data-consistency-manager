package oc.adventure.data.consistency.manager.controller;


import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.user.UserOutDto;
import oc.adventure.shared.components.dto.user.UserRegisterDto;
import oc.adventure.shared.components.dto.user.UserRegisterLightDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/ms/consistency/user/")
public class UserConsistenceController {

    private final RestTemplate restTemplate;

    private final String API = "api/v1/ms/";
    private final String USERMS_URL = "http://192.168.99.100:8080/user/";
    private final String ADDRESS_URL = "http://192.168.99.100:8080/address/";
    private final String COMMANDE_URL = "http://192.168.99.100:8080/commande/";

    @Autowired
    public UserConsistenceController(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.build();
    }

    @PutMapping(value    = "update/{externalId}",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserOutDto update(@RequestBody UserRegisterLightDto userOutDto, @PathVariable("externalId") String externalId, HttpServletRequest request){

        String urlUser = USERMS_URL + API + "user/update/" + externalId;

        //setting up the request headers
        //set up the authentication header
        String authorizationHeader = request.getHeader("Authorization");

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        requestHeaders.add("Authorization", authorizationHeader);

        //request entityCommande is created with request body and headers
        HttpEntity<UserRegisterLightDto> requestUserEntity = new HttpEntity<>(userOutDto, requestHeaders);

        ResponseEntity<UserOutDto> entityUser = restTemplate.exchange(
                urlUser,
                HttpMethod.PUT, requestUserEntity,
                UserOutDto.class);


        String urlUserAdress = ADDRESS_URL + API + "address/user/update/";

        UserInDto userInDto = new UserInDto();
        userInDto.setFirstName(userOutDto.getFirstName());
        userInDto.setLastName(userOutDto.getLastName());
        userInDto.setExternalId(externalId);

        //request entityCommande is created with request body and headers
        HttpEntity<UserInDto> requestUserAddressEntity = new HttpEntity<>(userInDto, requestHeaders);

        ResponseEntity<UserInDto> entity = restTemplate.exchange(
                urlUserAdress,
                HttpMethod.PUT, requestUserAddressEntity,
                UserInDto.class);

        String urlUserCommande = COMMANDE_URL + API + "commande/user/update/";

        //request entityCommande is created with request body and headers
        HttpEntity<UserInDto> requestUserCommandeEntity = new HttpEntity<>(userInDto, requestHeaders);

        ResponseEntity<UserInDto> entityCommande = restTemplate.exchange(
                urlUserCommande,
                HttpMethod.PUT, requestUserCommandeEntity,
                UserInDto.class);

        return entityUser.getBody();
    }

    @PostMapping(value    = "register/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserOutDto create(@RequestBody UserRegisterDto userRegisterDto){
        String urlUser = USERMS_URL + API + "user/register/";
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

//request entity is created with request body and headers
        HttpEntity<UserRegisterLightDto> requestUserEntity = new HttpEntity<>(userRegisterDto, requestHeaders);

        ResponseEntity<UserOutDto> entityUser = restTemplate.exchange(
                urlUser,
                HttpMethod.POST, requestUserEntity,
                UserOutDto.class);

        String urlUserAdress = ADDRESS_URL + API + "address/user/register/";

        UserInDto userInDto = new UserInDto();
        userInDto.setFirstName(userRegisterDto.getFirstName());
        userInDto.setLastName(userRegisterDto.getLastName());
        userInDto.setExternalId(Objects.requireNonNull(entityUser.getBody()).getExternalId());

        //request entityCommande is created with request body and headers
        HttpEntity<UserInDto> requestUserAddressEntity = new HttpEntity<>(userInDto, requestHeaders);

        ResponseEntity<UserInDto> entity = restTemplate.exchange(
                urlUserAdress,
                HttpMethod.POST, requestUserAddressEntity,
                UserInDto.class);

        String urlUserCommande = COMMANDE_URL + API + "commande/user/register";

        //request entityCommande is created with request body and headers
        HttpEntity<UserInDto> requestUserCommandeEntity = new HttpEntity<>(userInDto, requestHeaders);

        ResponseEntity<UserInDto> entityCommande = restTemplate.exchange(
                urlUserCommande,
                HttpMethod.POST, requestUserCommandeEntity,
                UserInDto.class);

        return entityUser.getBody();
    }

}
